package com.mobicare.employees.BackendPlenoViniciusAbreu.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    private static Employee employee;

    @Test
    void getAge() {
        assertEquals(22, employee.getAge());
        assertNotEquals(23, employee.getAge());
    }

    @BeforeAll
    static void before() {
        employee = new Employee();
        employee.setBirthdate(LocalDate.parse("1999-10-31"));
    }
}