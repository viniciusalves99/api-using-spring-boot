package com.mobicare.employees.BackendPlenoViniciusAbreu.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.AssertTrue;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    private static Department department;

    @Test
    void isValidToRegisterUnderAgeEmployee() {

        assertTrue(department.isValidToRegisterUnderAgeEmployee());

        department.getEmployees().remove(0);
        department.getEmployees().remove(0);
        department.getEmployees().remove(0);
        department.getEmployees().remove(0);
        department.getEmployees().remove(0);
        department.getEmployees().remove(0);

        assertFalse(department.isValidToRegisterUnderAgeEmployee());
    }

    @BeforeAll
    static void beforeClass() {
        department = new Department();

        department.setEmployees(new ArrayList<>());
        department.getEmployees().addAll(getEmployees(10, LocalDate.parse("1999-10-31")));

    }

    private static List<Employee> getEmployees(int count, LocalDate birthDate) {
        List<Employee> employees = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            Employee employee = new Employee();
            employee.setBirthdate(birthDate);
            employees.add(employee);
        }

        return employees;
    }
}