package com.mobicare.employees.BackendPlenoViniciusAbreu.dtos;

import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Employee;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeInputDTOTest {

    private static EmployeeInputDTO employeeInputDTO;

    @Test
    void getAge() {
        assertEquals(22, employeeInputDTO.getAge());
        assertNotEquals(23, employeeInputDTO.getAge());
    }

    @BeforeAll
    static void before() {
        employeeInputDTO = new EmployeeInputDTO();
        employeeInputDTO.setBirthdate(LocalDate.parse("1999-10-31"));
    }
}