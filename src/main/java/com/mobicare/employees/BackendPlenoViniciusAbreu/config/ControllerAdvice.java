package com.mobicare.employees.BackendPlenoViniciusAbreu.config;

import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.ErrorDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.exceptions.EmployeeNotFoundException;
import com.mobicare.employees.BackendPlenoViniciusAbreu.exceptions.EmployeeValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ControllerAdvice {

    @Autowired
    private MessageSource messageSource;

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handle(Exception exception) {
        if (exception instanceof MethodArgumentNotValidException)
            return new ResponseEntity<>(getValidationErrorsResponse((MethodArgumentNotValidException) exception), HttpStatus.BAD_REQUEST);
        if (exception instanceof EmployeeValidationException)
            return new ResponseEntity<>(getBusinesRulesErrorsResponse((EmployeeValidationException) exception), HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private List<String> getBusinesRulesErrorsResponse(EmployeeValidationException exception) {
        return exception.getErrors();
    }

    private List<ErrorDTO> getValidationErrorsResponse(MethodArgumentNotValidException exception) {
        List<ErrorDTO> errorsDTO = new ArrayList<>();

        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        fieldErrors.forEach(fieldError -> {
            String message = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
            ErrorDTO errorDTO = new ErrorDTO(fieldError.getField(), message);
            errorsDTO.add(errorDTO);
        });

        return errorsDTO;
    }
}
