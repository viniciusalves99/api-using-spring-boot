package com.mobicare.employees.BackendPlenoViniciusAbreu.dtos;

import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Employee;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class EmployeesGroupedByDepartmentOutputDTO {
    String department;
    List<EmployeeOutputDTO> employees;

    public EmployeesGroupedByDepartmentOutputDTO(String department, List<Employee> employees) {
        this.department = department;
        this.employees = employees.stream().map(EmployeeOutputDTO::new).collect(Collectors.toList());
    }
}
