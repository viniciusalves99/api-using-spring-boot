package com.mobicare.employees.BackendPlenoViniciusAbreu.dtos;

import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Period;

@Data
public class EmployeeInputDTO {
    @NotBlank
    @CPF
    private String cpf;
    @NotBlank
    private String name;
    @NotBlank
    private String phone;
    @NotBlank
    @Email
    private String email;
    @NotNull
    private LocalDate birthdate;

    public int getAge() {
        return Period.between(this.birthdate, LocalDate.now()).getYears();
    }
}
