package com.mobicare.employees.BackendPlenoViniciusAbreu.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDTO {

    String field;
    String message;
}
