package com.mobicare.employees.BackendPlenoViniciusAbreu.dtos;

import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Department;
import lombok.Data;

@Data
public class DepartmentOutputDTO {
    private Integer id;
    private String description;

    public DepartmentOutputDTO(Department department) {
        this.id = department.getId();
        this.description = department.getDescription();
    }
}