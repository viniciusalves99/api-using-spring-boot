package com.mobicare.employees.BackendPlenoViniciusAbreu.dtos;

import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Employee;
import lombok.Data;

@Data
public class EmployeeOutputDTO {
    private String name;
    private String email;
    private int age;

    public EmployeeOutputDTO(Employee employee) {
        this.name = employee.getName();
        this.email = employee.getEmail();
        this.age = employee.getAge();
    }
}
