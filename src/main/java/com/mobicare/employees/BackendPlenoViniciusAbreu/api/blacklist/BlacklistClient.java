package com.mobicare.employees.BackendPlenoViniciusAbreu.api.blacklist;

import com.mobicare.employees.BackendPlenoViniciusAbreu.api.blacklist.dtos.BlacklistClientSearchOutputDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "blacklist", url = "${feign.client.blacklist.url}")
public interface BlacklistClient {

    @GetMapping
    public ResponseEntity<List<BlacklistClientSearchOutputDTO>> search(@RequestParam String search);
}
