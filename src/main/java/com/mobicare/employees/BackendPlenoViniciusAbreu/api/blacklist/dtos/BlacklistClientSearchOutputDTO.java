package com.mobicare.employees.BackendPlenoViniciusAbreu.api.blacklist.dtos;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BlacklistClientSearchOutputDTO {

    private String id;
    private LocalDateTime createdAt;
    private String name;
    private String cpf;
}
