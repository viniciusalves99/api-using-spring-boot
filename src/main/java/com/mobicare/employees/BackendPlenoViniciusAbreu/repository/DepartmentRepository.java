package com.mobicare.employees.BackendPlenoViniciusAbreu.repository;

import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {
}
