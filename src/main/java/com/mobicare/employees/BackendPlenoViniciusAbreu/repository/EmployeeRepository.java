package com.mobicare.employees.BackendPlenoViniciusAbreu.repository;

import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    Optional<Employee> findByCpf(String cpf);

    boolean existsByCpf(String cpf);

    boolean existsByEmail(String cpf);

    void deleteAllByCpf(String cpf);
}
