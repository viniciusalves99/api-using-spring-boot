package com.mobicare.employees.BackendPlenoViniciusAbreu.resources;

import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.EmployeeInputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.EmployeeOutputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.EmployeesGroupedByDepartmentOutputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.exceptions.EmployeeNotFoundException;
import com.mobicare.employees.BackendPlenoViniciusAbreu.exceptions.EmployeeValidationException;
import com.mobicare.employees.BackendPlenoViniciusAbreu.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("employees")
public class EmployeeResource {

    @Autowired
    private EmployeeService employeeService;

    @CacheEvict(cacheNames = "employees")
    @PostMapping("create/{departmentId}")
    public ResponseEntity<?> createEmployee(@PathVariable Integer departmentId, @RequestBody @Valid EmployeeInputDTO employeeInputDTO) throws EmployeeValidationException {

        employeeService.createEmployee(departmentId, employeeInputDTO);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    @Cacheable("employees")
    public ResponseEntity<Page<EmployeesGroupedByDepartmentOutputDTO>> getEmployees(@PageableDefault(page = 0, size = 10, sort = "id", direction = Sort.Direction.ASC)
                                                                                                Pageable pageable) {

        Page<EmployeesGroupedByDepartmentOutputDTO> employees = employeeService.getEmployees(pageable);

        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @GetMapping("{cpf}")
    public ResponseEntity<EmployeeOutputDTO> getEmployeeByCpf(@PathVariable String cpf) throws EmployeeNotFoundException {

        EmployeeOutputDTO employee = employeeService.getEmployeeByCPF(cpf);

        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @DeleteMapping("{cpf}")
    public ResponseEntity<?> remove(@PathVariable String cpf) throws EmployeeNotFoundException {

        employeeService.removeEmployee(cpf);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
