package com.mobicare.employees.BackendPlenoViniciusAbreu.resources;

import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.DepartmentOutputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.services.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("department")
public class DepartmentResource {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("getAll")
    public ResponseEntity<List<DepartmentOutputDTO>> getDepartments() {
        List<DepartmentOutputDTO> departmentOutputDTOS = departmentService.getAll();

        return new ResponseEntity<>(departmentOutputDTOS, HttpStatus.OK);
    }
}
