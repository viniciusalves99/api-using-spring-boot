package com.mobicare.employees.BackendPlenoViniciusAbreu.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDateTime createdAt;
    private String name;
    private String description;
    @OneToMany(mappedBy = "department", fetch = FetchType.EAGER)
    private List<Employee> employees;

    public boolean isValidToRegisterUnderAgeEmployee() {
        List<Employee> underAgeEmployees = this.employees.stream().filter(employee -> employee.getAge() < 18).collect(Collectors.toList());
        return employees.size() * 0.2 >= underAgeEmployees.size() + 1;
    }
}
