package com.mobicare.employees.BackendPlenoViniciusAbreu.model;

import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.EmployeeInputDTO;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

@Entity
@Data
@NoArgsConstructor
public class Employee {

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDateTime createdAt;
    private String cpf;
    private String name;
    private LocalDate birthdate;
    private String phone;
    private String email;
    @ManyToOne(cascade = CascadeType.ALL)
    private Department department;

    public Employee(EmployeeInputDTO employeeInputDTO, Department department) {
        this.createdAt = LocalDateTime.now();
        this.cpf = employeeInputDTO.getCpf();
        this.name = employeeInputDTO.getName();
        this.birthdate = employeeInputDTO.getBirthdate();
        this.phone = employeeInputDTO.getPhone();
        this.email = employeeInputDTO.getEmail();
        this.department = department;
    }

    public int getAge() {
        return Period.between(this.birthdate, LocalDate.now()).getYears();
    }
}
