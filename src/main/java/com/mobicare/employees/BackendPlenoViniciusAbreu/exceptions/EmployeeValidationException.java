package com.mobicare.employees.BackendPlenoViniciusAbreu.exceptions;

import lombok.Data;

import java.util.List;

@Data
public class EmployeeValidationException extends Exception {

    List<String> errors;

    public EmployeeValidationException(List<String> errors) {
        this.errors = errors;
    }
}
