package com.mobicare.employees.BackendPlenoViniciusAbreu.exceptions;

import lombok.Data;

import java.util.List;

@Data
public class EmployeeNotFoundException extends Exception {

    public EmployeeNotFoundException() {
        super("Employee não encontrado");
    }
}
