package com.mobicare.employees.BackendPlenoViniciusAbreu.services;

import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.DepartmentOutputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Department;
import com.mobicare.employees.BackendPlenoViniciusAbreu.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public List<DepartmentOutputDTO> getAll() {
        List<Department> departments = departmentRepository.findAll();

        return departments.stream().map(DepartmentOutputDTO::new).collect(Collectors.toList());
    }
}
