package com.mobicare.employees.BackendPlenoViniciusAbreu.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

@Service
public class CacheService {

    @Autowired
    private CacheManager cacheManager;

    public void clearCache(String name) {
        cacheManager.getCache(name).clear();
    }
}
