package com.mobicare.employees.BackendPlenoViniciusAbreu.services;

import com.mobicare.employees.BackendPlenoViniciusAbreu.api.blacklist.BlacklistClient;
import com.mobicare.employees.BackendPlenoViniciusAbreu.api.blacklist.dtos.BlacklistClientSearchOutputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.EmployeeInputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.EmployeeOutputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.dtos.EmployeesGroupedByDepartmentOutputDTO;
import com.mobicare.employees.BackendPlenoViniciusAbreu.exceptions.EmployeeNotFoundException;
import com.mobicare.employees.BackendPlenoViniciusAbreu.exceptions.EmployeeValidationException;
import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Department;
import com.mobicare.employees.BackendPlenoViniciusAbreu.model.Employee;
import com.mobicare.employees.BackendPlenoViniciusAbreu.repository.DepartmentRepository;
import com.mobicare.employees.BackendPlenoViniciusAbreu.repository.EmployeeRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private BlacklistClient blacklistClient;

    @Autowired
    private CacheService cacheService;

    public void createEmployee(Integer departmentId, EmployeeInputDTO employeeInputDTO) throws EmployeeValidationException {
        Optional<Department> departmentOptional = departmentRepository.findById(departmentId);

        validateRegisterData(employeeInputDTO, departmentOptional.get());

        Employee employee = new Employee(employeeInputDTO, departmentOptional.get());
        employeeRepository.save(employee);

        cacheService.clearCache("employees");
    }

    private void validateRegisterData(EmployeeInputDTO employeeInputDTO, Department department) throws EmployeeValidationException {
        List<String> errors = new ArrayList<>();

        if(department == null)
            errors.add("Setor não encontrado");

        boolean existsByCpf = employeeRepository.existsByCpf(employeeInputDTO.getCpf());
        if(existsByCpf)
            errors.add("Cpf já está cadastrado na empresa");

        boolean existsByEmail = employeeRepository.existsByEmail(employeeInputDTO.getEmail());
        if(existsByEmail)
            errors.add("Email já está cadastrado na empresa");

        if(employeeInputDTO.getAge() < 18) {
            if (!department.isValidToRegisterUnderAgeEmployee())
                errors.add("Somente 20% do setor pode ser menor de idade");
        } else if (employeeInputDTO.getAge() > 65) {
            if (!isValidToRegisterOldEmployee(employeeInputDTO))
                errors.add("Somente 20% da empresa pode ser maior de 65 anos");
        }

        ResponseEntity<List<BlacklistClientSearchOutputDTO>> search = blacklistClient.search(employeeInputDTO.getCpf());

        if(!CollectionUtils.isEmpty(search.getBody()))
            errors.add("Funcionário se encontra da blacklist");

        if(!errors.isEmpty())
            throw new EmployeeValidationException(errors);
    }

    private boolean isValidToRegisterOldEmployee(EmployeeInputDTO employeeInputDTO) {
        List<Employee> employees = employeeRepository.findAll();

        int oldEmployeesNumber = employees.stream().filter(employee -> employee.getAge() > 65).collect(Collectors.toList()).size();

        return employees.size() * 0.2 >= oldEmployeesNumber + 1;
    }

    public Page<EmployeesGroupedByDepartmentOutputDTO> getEmployees(Pageable pageable) {
        Page<Employee> employees = employeeRepository.findAll(pageable);
        List<EmployeesGroupedByDepartmentOutputDTO> employeesGroupedByDepartment = new ArrayList<>();

        Map<String, List<Employee>> groupedEmployees = employees.getContent().stream().collect(Collectors.groupingBy(employee -> employee.getDepartment().getDescription()));
        groupedEmployees.forEach((description, employee) -> employeesGroupedByDepartment.add(new EmployeesGroupedByDepartmentOutputDTO(description, employee)));

        return new PageImpl<>(employeesGroupedByDepartment, pageable, employees.getTotalElements());
    }

    public EmployeeOutputDTO getEmployeeByCPF(String cpf) throws EmployeeNotFoundException {
        Optional<Employee> employee = employeeRepository.findByCpf(cpf);

        if(employee.isEmpty())
            throw new EmployeeNotFoundException();

        return new EmployeeOutputDTO(employee.get());
    }

    @Transactional
    public void removeEmployee(String cpf) throws EmployeeNotFoundException {
        if(!employeeRepository.existsByCpf(cpf))
            throw new EmployeeNotFoundException();

        employeeRepository.deleteAllByCpf(cpf);
        cacheService.clearCache("employees");
    }
}
