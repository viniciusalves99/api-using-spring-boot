package com.mobicare.employees.BackendPlenoViniciusAbreu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class BackendPlenoViniciusAbreuApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendPlenoViniciusAbreuApplication.class, args);
	}

}
