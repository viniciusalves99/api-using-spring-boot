create table if not exists department (
    id serial primary key,
    created_at timestamp not null,
    name varchar not null,
    description varchar not null
);

create table if not exists employee (
    id serial primary key,
    created_at timestamp not null,
    cpf varchar not null,
    name varchar not null,
    birthdate date not null,
    phone varchar not null,
    email varchar not null,
    department_id integer references department(id));